(function () {
  var apiConsumer  = angular.module("appConsumer", ['ngResource', 'ngRoute']);  

  apiConsumer.config(function($routeProvider) {

  
    $routeProvider.when('/api/messages', {
      templateUrl: 'partials/messages.html',
      controller: 'apiListController'
    });
     $routeProvider.when('/api/addMessage', {
      templateUrl: 'partials/addMessage.html',
      controller: 'apiAddController'
    });

      $routeProvider.when('/api/messages/:messageId', {
      templateUrl: 'partials/updateMessage.html',
      controller: 'apiUpdateController'
    });

  
    
    $routeProvider.otherwise({redirectTo: '/api/messages'});
  });

// api  resource 
  apiConsumer.factory('MessageService', function ($resource) {
      return $resource('http://localhost:8080/messages-api/webapi/messages/:messageId',
          {
            messageId: "@messageId"
          }
          ,
          {
            update:{
                method:'PUT'
            }
          }
        );
  });

  // api  controller 

    apiConsumer.controller("apiListController" , 
                           [ "$scope" , "MessageService", function ($scope ,MessageService){
    		$scope.messages =  null  ;
    		$scope.message = [] ; 
          var msgs = []; 
        $scope.newMessage = {
          messageTitle : "" , 
          messageBody : ""
        }

    	 $scope.getMessages  = function() {
    		  
           return  MessageService.query() ; 
    	} 

      $scope.messages  = $scope.getMessages ();  


   	  $scope.deleteMessage = function (msgId )
    	{
        
            MessageService.delete({messageId : msgId}) ; 
      } 
      
    }]) ; 

 apiConsumer.controller("apiAddController" , 
                           [ "$scope" , "MessageService", function ($scope , MessageService){

        $scope.newMessage = {
          messageTitle : "" , 
          messageBody : ""
        }

      $scope.addMessage = function ()
      {
          MessageService.save($scope.newMessage) ;  
          console.log($scope.newMessage); 
      }
      
    }]) ; 

  apiConsumer.controller("apiDetailController" , 
                         [ "$scope", "$routeParams", "MessageService", 
                         function ($scope ,$routeParams, MessageService){

        $scope.message = {
          messageBody : "", 
          messageTitle : ""
        }
 
       $scope.getMessage = function ()
      {
         return  MessageService.get({messageId: $routeParams.messageId});
      }

      $scope.message = $scope.getMessage();  
      
    }]) ; 

   apiConsumer.controller("apiUpdateController" , 
                         [ "$scope", "$routeParams", "MessageService", 
                         function ($scope ,$routeParams, MessageService){


        $scope.message = {
          messageBody : "", 
          messageTitle : "",
          messageId : $routeParams.messageId
        }

       $scope.getMessage = function ()
      {
         return  MessageService.get({messageId: $routeParams.messageId});
      }

      $scope.message = $scope.getMessage();  


      $scope.updateMessage = function ()
      {
        console.log($scope.message) ; 


         MessageService.update(
                              {
                                messageId: $scope.message.messageId
                              }
                              , 
                              {
                                messageTitle: $scope.message.messageTitle, 
                                messageBody: $scope.message.messageBody 
                              }
                              );
      }
        
    }]) ; 

})() ; 