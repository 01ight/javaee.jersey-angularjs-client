package com.jersey.restapi.messages_api;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rest.api.dao.ConnectionDB;
import com.rest.api.dao.MessageDAO;
import com.rest.api.models.Message;


@Path("messages")
public class MessageResource {
	
	@GET
	@Consumes({MediaType.TEXT_PLAIN}) 
	@Produces({MediaType.APPLICATION_JSON}) 
	public ArrayList<Message> getMessages() { 
		
		System.out.println(System.getProperty("catalina.base"));
		 MessageDAO  message = null ;
		 ArrayList<Message> messagesList = new ArrayList<Message>() ; 
		try {
			message = new MessageDAO(ConnectionDB.getInstance()) ;
			messagesList = message.list() ; 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	 

	 return  messagesList ; 
	}
	
	@GET
	@Path("/{messageId}") 
	@Consumes({MediaType.TEXT_PLAIN}) 
	@Produces({MediaType.APPLICATION_JSON}) 
	public Message getMessage(@PathParam("messageId") int messageId ) { 
		
	
		 MessageDAO  message = null ;
		 
		 Message msg = null ;  
		try {
			message = new MessageDAO(ConnectionDB.getInstance()) ;
			msg  = message.find(messageId) ; 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return  msg  ; 
	}

	@POST  
	@Consumes({MediaType.APPLICATION_JSON}) 
	@Produces({MediaType.APPLICATION_JSON}) 
	public ArrayList<Message>  addMessage(Message newMessage  ) 
	{
		System.out.print(newMessage.toString());
		ArrayList<Message> messagesList  = new ArrayList<Message> () ;
		 MessageDAO  message = null ;
			try {
			message = new MessageDAO(ConnectionDB.getInstance()) ;
			message.create(newMessage) ; 
			messagesList = message.list(); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return messagesList ;
	}

	@PUT
	@Path("/{messageId}") 
	@Consumes({MediaType.APPLICATION_JSON}) 
	@Produces({MediaType.APPLICATION_JSON}) 
	public  Message  updateMessage( @PathParam("messageId") int messageId  , Message newMessage) 
	{
		MessageDAO  message = null ; 
		Message msg = null ; 
		try {
			message = new MessageDAO(ConnectionDB.getInstance()) ;
			Message m  = message.find(messageId) ; 
			 m.setMessageBody(newMessage.getMessageBody()) ; 
			 m.setMessageTitle(newMessage.getMessageTitle()) ; 
			message.update(m) ; 
			msg = message.find(messageId) ; 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg  ; 
	}

	@DELETE
	@Path("/{messageId}")
	@Produces({MediaType.TEXT_PLAIN}) 
	public  Response deleteMessage(@PathParam("messageId") int messageId )
	{
				 MessageDAO  message = null ; 
		 
		try {
			message = new MessageDAO(ConnectionDB.getInstance()) ;
			message.delete(messageId) ; 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return Response.status(200)
				.build();
	}
	@OPTIONS
	public Response options() {
	    return Response
	            .status(Response.Status.OK)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, PATCH")
	            .header("Access-Control-Allow-Credentials",true)
	            .build();
	}
}