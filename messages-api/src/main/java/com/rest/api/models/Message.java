package  com.rest.api.models  ;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class Message {

	private  int     messageId  ; 
	private  String  messageBody  ; 
	private  String  messageTitle ;
	
	public Message() 
	{
		
	}
	
	public  Message (Integer messageId , String  messageTitle,  String messageBody)
	{
		this.messageId  = messageId ; 
		this .messageTitle = messageTitle ; 
		this .messageBody = messageBody ; 
		
	}
	
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	public String getMessageTitle() {
		return messageTitle;
	}
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	@Override
	public String toString() {
		return "Message [messageId=" + messageId + ", messageBody=" + messageBody + ", messageTitle=" + messageTitle
				+ "]";
	} 

}