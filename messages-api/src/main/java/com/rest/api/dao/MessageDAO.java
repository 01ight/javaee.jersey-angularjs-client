package com.rest.api.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.rest.api.models.Message;

public class MessageDAO extends DAO<Message>{

	public MessageDAO () {
		
	}
	public  MessageDAO ( Connection  connection) {
		super(connection) ;
	}
	
	@Override
	public boolean create(Message obj) {
		// TODO Auto-generated method stub

		try {
			
				PreparedStatement prepare  = this.connect
											     .prepareStatement("INSERT INTO  messages ( message_title , message_body)"
											     		+ "VALUES (?,?)") ; 
				
				prepare.setString(1, obj.getMessageTitle());
				prepare.setString(2, obj.getMessageBody());
			
				prepare.executeUpdate() ; 
		}
		catch (SQLException  e )
		{
			e.printStackTrace();
		}
		return true ; 
	}

	@Override
	public boolean delete( int id ) {
		try {
				
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
						.executeQuery("DELETE FROM  messages WHERE message_id = "+ id) ;
		}
		catch (SQLException  e )
		{
			e.printStackTrace();
		}
		return  true ;
	}

	@Override
	public boolean update(Message obj ) {
		
		try {
			
			
			PreparedStatement prepare  = this.connect
										     .prepareStatement("UPDATE messages SET message_title = ? , message_body= ? WHERE message_id = ?") ; 
			
			prepare.setString(1, obj.getMessageTitle());
				prepare.setString(2, obj.getMessageBody());
				prepare.setInt(3, obj.getMessageId());
			
			prepare.executeUpdate() ; 
	}
	catch (SQLException  e )
	{
		e.printStackTrace();
	}
		return  true ; 
	}

	@Override
	public Message find(int id) {
		
		Message Message = new Message() ;
		
		try {
				ResultSet result = this.connect
										.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
										.executeQuery("SELECT * FROM messages WHERE message_id = " + id) ; 
				if(result.first() )
				{
					
					Message = new Message(
										  result.getInt("message_id"), 
										  result.getString("message_title"), 
										  result.getString("message_body")) ; 
							
					
					
				}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
		return  Message  ; 
	}

	@Override
	public ArrayList<Message> list() {
	
		ArrayList <Message>  MessageList  = new ArrayList<Message>() ;
		Message  Message = new Message() ; 
		try {
			ResultSet result = this.connect
					.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
					.executeQuery("SELECT * FROM messages  ") ; 
			while (result.next() )
			{
				Message = new Message(result.getInt("message_id"), 
						result.getString("message_title"), 
						result.getString("message_body")) ;
				MessageList.add(Message) ; 
			}
		}
		catch (SQLException  e) 
		{
			e.printStackTrace();
		}
		return MessageList  ; 
		
	}

	

}
